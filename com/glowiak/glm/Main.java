package com.glowiak.glm;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Dimension;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.util.Random;
import java.util.ArrayList;

import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.URL;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Main extends JFrame
{
    protected static Main me;
    protected static Image BACKGROUND;
    
    private static JButton B_PLAY;
    private static JButton B_OPTS;
    
    protected static JComboBox CB_PACK;
    protected static JTextField TF_NICK;
    
    protected static LauncherPanel lp;
    
    protected static ArrayList<Pack> packs;
    public static int selectedPack;
    
    protected static String username;
    
    protected static final int VMA = 1;
    protected static final int VMI = 0;
    protected static final int VHO = 0;
    
    public static void main(String[] args)
    {
        me = new Main();
    }
    
    public Main()
    {
        super();
        
        this.setTitle("Minecraft Launcher");
        this.setSize(800, 600);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setIconImage(new ImageIcon(this.getClass().getResource("/resources/icons/icon.png")).getImage());
        
        lp = new LauncherPanel();
        packs = new ArrayList<Pack>();
        
        int bgc = new Random().nextInt(8) + 1;
        BACKGROUND = new ImageIcon(this.getClass().getResource("/resources/backgrounds/" + bgc + ".png")).getImage().getScaledInstance(800, 600, Image.SCALE_DEFAULT);
        
        B_PLAY = new JButton("Play");
        B_OPTS = new JButton("Pack Options");
        
        CB_PACK = new JComboBox();
        TF_NICK = new JTextField();
        
        B_PLAY.setBounds(650, 520, 120, 35);
        B_OPTS.setBounds(500, 520, 140, 35);
        
        CB_PACK.setBounds(500, 480, 270, 30);
        TF_NICK.setBounds(500, 445, 270, 30);
        
        lp.add(B_PLAY);
        lp.add(B_OPTS);
        lp.add(CB_PACK);
        lp.add(TF_NICK);
        
        this.add(lp);
        
        loadDefaultPacks();
        refreshPacks();
        
        B_PLAY.addActionListener(new GameRunner());
        
        new File(Util.getMinecraftDir() + "/glm").mkdirs();
        
        if (new File(Util.getMinecraftDir() + "/glm/username.txt").exists())
        {
            try {
                FileReader fr = new FileReader(Util.getMinecraftDir() + "/glm/username.txt");
                BufferedReader br = new BufferedReader(fr);
                username = br.readLine();
                TF_NICK.setText(username);
                br.close();
                fr.close();
            } catch (IOException ioe) { ioe.printStackTrace(); }
        }
        if (new File(Util.getMinecraftDir() + "/glm/currentPack.txt").exists())
        {
            try {
                FileReader fr = new FileReader(Util.getMinecraftDir() + "/glm/currentPack.txt");
                BufferedReader br = new BufferedReader(fr);
                selectedPack = Integer.parseInt(br.readLine());
                CB_PACK.setSelectedIndex(selectedPack);
                br.close();
                fr.close();
            } catch (IOException ioe) { ioe.printStackTrace(); }
        } else
        {
            selectedPack = 0;
        }
        
        while (true)
        {
            lp.repaint();
            
            try { Thread.sleep(5); } catch (InterruptedException ie) { ie.printStackTrace(); }
        }
    }
    protected boolean loadDefaultPacks()
    {
        loadInternalPack("/resources/packs/lotrfa.glm");
        loadInternalPack("/resources/packs/gc152.glm");
        loadInternalPack("/resources/packs/lotr152.glm");
        loadInternalPack("/resources/packs/mocoal125.glm");
        loadInternalPack("/resources/packs/vanilla777181.glm");
        loadInternalPack("/resources/packs/vanilla181.glm");
        loadInternalPack("/resources/packs/vanilla1122.glm");
        
        return true;
    }
    protected boolean loadUserPacks()
    {
        return true;
    }
    protected void loadInternalPack(String resname)
    {
        packs.add(new Pack());
        packs.get(packs.size() - 1).load(this.getClass().getResourceAsStream(resname));
    }
    protected void refreshPacks()
    {
        CB_PACK.removeAllItems();
        
        for (int i = 0; i < packs.size(); i++)
        {
            CB_PACK.addItem(packs.get(i).name);
        }
    }
}
class GameRunner implements ActionListener
{
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (Main.TF_NICK.getText().equals(""))
        {
            JOptionPane.showMessageDialog(Main.me, "Username cannot be empty!");
            return;
        }
        
        try {
            FileWriter fw = new FileWriter(Util.getMinecraftDir() + "/glm/username.txt");
            Main.username = Main.TF_NICK.getText();
            fw.write(Main.username);
            fw.close();
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            JOptionPane.showMessageDialog(Main.me, "Could not save username");
        }

        Main.selectedPack = Main.CB_PACK.getSelectedIndex();
        
        try {
            FileWriter fw = new FileWriter(Util.getMinecraftDir() + "/glm/currentPack.txt");
            fw.write(String.format("%d", Main.selectedPack));
            fw.close();
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            JOptionPane.showMessageDialog(Main.me, "Could not save current pack");
        }
        
        boolean result;
        
        result = Main.packs.get(Main.selectedPack).fetchLibs();
        
        if (!result)
        {
            JOptionPane.showMessageDialog(Main.me, "An error happened when trying to download libraries.");
            return;
        }
        
        result = Main.packs.get(Main.selectedPack).fetchMods();
        
        if (!result)
        {
            JOptionPane.showMessageDialog(Main.me, "An error happened when trying to download mods.");
            return;
        }
        
        result = Main.packs.get(Main.selectedPack).unpackNatives();
        
        if (!result)
        {
            JOptionPane.showMessageDialog(Main.me, "An error happened when trying to extract natives.");
            return;
        }
        
        result = Main.packs.get(Main.selectedPack).unpackJars();
        
        if (!result)
        {
            JOptionPane.showMessageDialog(Main.me, "An error happened when trying to extract game jars.");
            return;
        }
        
        result = Main.packs.get(Main.selectedPack).installResources();
        
        if (!result)
        {
            JOptionPane.showMessageDialog(Main.me, "An erro happened when trying to install assets.");
            return;
        }
        
        result = Main.packs.get(Main.selectedPack).installForgeLibs();
        
        if (!result)
        {
            JOptionPane.showMessageDialog(Main.me, "An erro happened when trying to install Forge libraries.");
            return;
        }
        
        String txe = Main.packs.get(Main.selectedPack).generateLaunchCommand();
        
        System.out.println("Command to be executed: " + txe);
        
        try {
            Runtime.getRuntime().exec(txe);
        } catch (IOException ioe)
        {
            System.out.println(ioe);
            JOptionPane.showMessageDialog(Main.me, "Could not start Java. Details in console.");
            return;
        }
    }
}
class LauncherPanel extends JPanel
{
    public LauncherPanel()
    {
        super();
        
        this.setDoubleBuffered(true);
        this.setFocusable(true);
        this.setLayout(null);
        this.setBounds(0, 0, 800, 600);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        g.drawImage(Main.BACKGROUND, 0, 0, null);
        
        g.setColor(Color.white);
        
        g.drawString(Main.VMA + "." + Main.VMI + "." + Main.VHO, 750, 15);
        
        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * 1.4F));
        g.drawString("Pack:", 450, 500);
        g.drawString("Nick:", 450, 465);
        g.drawString("glowiak's minecraft launcher", 500, 440);
        
        g.drawRect(445, 420, 325, 135);
    }
}
class Buffer
{
}
class Pack
{
    protected String name;
    protected String codename;
    protected int version;
    private Version lv;
    protected int memory;
    protected String main_class;
    protected String classpath;
    protected ArrayList<String> cps;
    protected ArrayList<String> mods;
    protected ArrayList<String> coremods;
    protected ArrayList<String> cps_coremods;
    protected ArrayList<String> cps_classpath;
    
    public Pack()
    {
        this.name = "Empty";
        this.codename = "empty";
        this.version = 152;
        this.memory = 512;
        this.main_class = null;
        this.classpath = "";
        this.cps = new ArrayList<String>();
        this.mods = new ArrayList<String>();
        this.coremods = new ArrayList<String>();
        this.cps_coremods = new ArrayList<String>();
        this.cps_classpath = new ArrayList<String>();
    }
    protected String getDirectory()
    {
        return Util.getMinecraftDir() + "/packs/" + this.codename;
    }
    protected String getMinecraftDir()
    {
        return getDirectory() + "/minecraft";
    }
    
    protected boolean load(InputStream url)
    {
        try {
            InputStreamReader fr = null;
            try {
                fr = new InputStreamReader(url);
            } catch (Exception e) { e.printStackTrace(); }
            BufferedReader br = new BufferedReader(fr);
        
            String s;
            while ((s = br.readLine()) != null)
            {
                if (s.contains("Name="))
                {
                    this.name = s.replace("Name=", "");
                } else
                if (s.contains("Codename="))
                {
                    this.codename = s.replace("Codename=", "");
                } else
                if (s.contains("Version="))
                {
                    this.version = Integer.parseInt(s.replace("Version=", ""));
                } else
                if (s.contains("Memory="))
                {
                    this.memory = Integer.parseInt(s.replace("Memory=", ""));
                } else
                if (s.contains("MainClass="))
                {
                    this.main_class = s.replace("MainClass=", "");
                } else
                if (s.contains("Classpath+="))
                {
                    this.classpath += Util.getSeparator();
                    this.classpath += getMinecraftDir() + "/packs/" + this.name;
                    this.classpath += "/cpoverride/";
                    this.classpath += s.replace("Classpath+=", "");
                    this.cps_classpath.add(s.replace("Classpath+=", ""));
                } else
                if (s.contains("Mods+="))
                {
                    this.cps.add(s.replace("Mods+=", ""));
                    this.mods.add(Util.getFileFromUrl(s.replace("Mods+=", "")));
                }
                if (s.contains("Coremods+="))
                {
                    this.cps_coremods.add(s.replace("Coremods+=", ""));
                    this.coremods.add(Util.getFileFromUrl(s.replace("Coremods+=", "")));
                }
            }
        
            br.close();
            br.close();
            
            init();
            
            return true;
        } catch (IOException ioe) { ioe.printStackTrace(); return false; }
    }
    protected void init()
    {
        new File(getMinecraftDir()).mkdirs();
        new File(getMinecraftDir() + "/bin").mkdirs();
        new File(getMinecraftDir() + "/bin/natives").mkdirs();
        new File(getMinecraftDir() + "/bin/libs").mkdirs();
        new File(getMinecraftDir() + "/bin/minecraft").mkdirs();
        new File(getMinecraftDir() + "/mods").mkdirs();
        new File(getMinecraftDir() + "/coremods").mkdirs();
        new File(getMinecraftDir() + "/cpoverride").mkdirs();
        new File(getMinecraftDir() + "/resources").mkdirs();
        new File(getMinecraftDir() + "/lib").mkdirs();
    }
    protected boolean fetchMods()
    {
        for (int i = 0; i < this.cps.size(); i++)
        {
            boolean result = Util.fetch(this.cps.get(i), this.getMinecraftDir() + "/mods/" + this.mods.get(i));
            if (!result)
            {
                return false;
            }
        }
        for (int i = 0; i < this.cps_coremods.size(); i++)
        {
            boolean result = Util.fetch(this.cps_coremods.get(i), this.getMinecraftDir() + "/mods/" + this.coremods.get(i));
            if (!result)
            {
                return false;
            }
        }
        for (int i = 0; i < this.cps_classpath.size(); i++)
        {
            boolean result = Util.fetch(this.cps_classpath.get(i), this.getMinecraftDir() + "/cpoverride/" + Util.getFileFromUrl(this.cps_classpath.get(i)));
            if (!result)
            {
                return false;
            }
        }
        return true;
    }
    protected boolean fetchLibs()
    {
        Version v = null;
        
        switch (this.version)
        {
            case 152:
                v = new V152();
                break;
            case 125:
                v = new V125();
                break;
            case 777181:
                v = new V777181();
                break;
        }
        
        this.lv = v;
        
        for (int i = 0; i < v.libs.length; i++)
        {
            boolean result = Util.fetch(v.libs[i], getMinecraftDir() + "/bin/libs/" + Util.getFileFromUrl(v.libs[i]));
            if (!result)
            {
                return false;
            }
        }
        for (int i = 0; i < v.natives.length; i++)
        {
            boolean result = Util.fetch(v.natives[i], getMinecraftDir() + "/bin/libs/" + Util.getFileFromUrl(v.natives[i]));
            if (!result)
            {
                return false;
            }
        }
        
        boolean result = Util.fetch(v.jar, getMinecraftDir() + "/bin/minecraft.jar");
        if (!result)
        {
            return false;
        }
        
        return true;
    }
    protected boolean unpackNatives()
    {
        for (File f : new File(getMinecraftDir() + "/bin/libs").listFiles())
        {
            if (f.getPath().contains("lwjgl-platform") || f.getPath().contains("jinput-platform")) // TBA: automatic native detection
            {
                try { // TBA: extract natives for specific OS only
                    Util.unzip(f.getPath(), getMinecraftDir() + "/bin/natives");
                } catch (IOException ioe)
                {
                    System.out.println(ioe);
                    return false;
                }
            }
        }
        
        return true;
    }
    protected boolean unpackJars()
    {
        new File(getMinecraftDir() + "/bin/minecraft/META-INF").mkdirs();
        new File(getMinecraftDir() + "/bin/minecraft/net/minecraft/client").mkdirs();
        
        try {
            Util.unzip(getMinecraftDir() + "/bin/minecraft.jar", getMinecraftDir() + "/bin/minecraft");
        } catch (IOException ioe)
        {
            System.out.println(ioe);
            return false;
        }
        
        for (File f : new File(getMinecraftDir() + "/cpoverride").listFiles())
        {
            try {
                Util.unzip(f.getPath(), getMinecraftDir() + "/bin/minecraft");
            } catch (IOException ioe)
            {
                System.out.println(ioe);
                return false;
            }
        }
        
        return true;
    }
    protected boolean installResources()
    {
        boolean result;
        
        result = Util.fetch(this.lv.assets, getMinecraftDir() + "/resources/assets.zip");
        
        if (!result)
        {
            return false;
        }
        try {
            Util.unzip(getMinecraftDir() + "/resources/assets.zip", getMinecraftDir() + "/resources");
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            return false;
        }
        
        return true;
    }
    protected boolean installForgeLibs() // because old forge needs that
    {
        if (this.lv.lib != null)
        {
            boolean result = Util.fetch(this.lv.lib, getMinecraftDir() + "/lib/lib.zip");
            if (!result)
            {
                return false;
            }
            try {
                Util.unzip(getMinecraftDir() + "/lib/lib.zip", getMinecraftDir() + "/lib");
            } catch (IOException ioe)
            {
                ioe.printStackTrace();
                return false;
            }
        }
        return true;
    }
    protected String build_classpath()
    {
        String w = "";
        
        for (File f : new File(getMinecraftDir() + "/bin/libs").listFiles())
        {
            w += Util.getSeparator();
            w += f.getPath();
        }
        
        w = w.substring(1, w.length());
        
        w+= Util.getSeparator();
        w+= getMinecraftDir() + "/bin/minecraft";
        
        return w;
    }
    protected String generateLaunchCommand()
    {
        String w = "";
        
        w+= Util.getRunningJava();
        w+= " -Xmn";
        w+= Main.packs.get(Main.selectedPack).memory + "M";
        w+= " -Xmx";
        w+= Main.packs.get(Main.selectedPack).memory + "M";
        w+= " -Dfile.encoding=UTF-8";
        w+= " -Dminecraft.launcher.brand=GLM";
        w+= " -Dminecraft.launcher.version=" + Main.VMA + "." + Main.VMI + "." + Main.VHO;
        w+= " -Djava.library.path=" + getMinecraftDir() + "/bin/natives";
        w+= " -cp " + build_classpath();
        w+= " " + this.main_class;
        w+= " " + Main.username;
        w+= " as --gameDir " + getMinecraftDir();
        w+= " --username " + Main.username;
        w+= " --version " + this.version;
        w+= " --userProperties {}";
        w+= " --assetsDir " + getMinecraftDir() + "/resources";
        w+= " --assetIndex " + this.lv.assetIndex;
        w+= " --accessToken 12";
        w+= " --userType legacy";
        
        return w;
    }
}
class Util
{
    protected static String getRunningJava()
    {
        String javaPath = null;
        String os = null;
        if (System.getProperty("os.name").contains("indows"))
        {
            os = "windows";
        } else if (System.getProperty("os.name").toLowerCase().contains("mac os"))
        {
            os = "osx";
        } else
        {
            os = System.getProperty("os.name").toLowerCase();
        }
        
        String javaHome = System.getProperty("java.home");
        if (os.equals("windows"))
        {
            javaPath = javaHome + "/bin/javaw.exe";
        } else
        {
            javaPath = javaHome + "/bin/java";
        }
        return javaPath;
    }
    protected static String getSeparator()
    {
        if (System.getProperty("os.name").contains("indow"))
        {
            return ";";
        } else
        {
            return ":";
        }
    }
    protected static String getFileFromUrl(String url)
    {
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }
    protected static String getMinecraftDir()
    {
        if (System.getProperty("os.name").contains("indows"))
        {
            return System.getenv("APPDATA") + "/.minecraft";
        } else
        if (System.getProperty("os.name").contains("inux") || System.getProperty("os.name").toLowerCase().contains("bsd"))
        {
            return System.getenv("HOME") + "/.minecraft";
        } else
        {
            return null;
        }
    }
    protected static boolean fetch(String url, String dst)
    {
        try {
            File ie = new File(dst);
            if (!ie.exists()) {
                System.out.println("--> Downloading " + url + " to " + dst);
                URL u1 = new URL(url);
                Path u2 = Paths.get(dst);
                InputStream is = u1.openStream();
                try {
                    Files.copy(is, u2, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ioe) { System.out.println(ioe); }
            }
            return true;
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            return false;
        }
    }
    private static final int BUFFER_SIZE = 4096;
    protected static void unzip(String zfp, String dfp) throws IOException {
        File dest = new File(dfp);
        if (!dest.exists()) {
            dest.mkdir();
        }
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zfp));
        ZipEntry ze = zis.getNextEntry();

        while (ze != null) {
            String File_Path = dfp + File.separator + ze.getName();
            if (!ze.isDirectory()) {
                if (ze.getName().contains("/"))
                {
                    new File(dfp + File.separator + ze.getName().substring(0, ze.getName().lastIndexOf('/'))).mkdirs();
                }
                extractFile(zis, File_Path);
            } else {
                new File(File_Path).mkdirs();
            }
            zis.closeEntry();
            ze = zis.getNextEntry();
        }
        zis.close();
    }

    private static void extractFile(ZipInputStream zis, String path) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path));
        byte[] bytes = new byte[BUFFER_SIZE];
        int rb = 0;
        while ((rb = zis.read(bytes)) != -1) {
            bos.write(bytes, 0, rb);
        }
        bos.close();
    }
    protected static Version getVersion(int ver)
    {
        switch (ver)
        {
            case 152:
                return new V152();
            case 125:
                return new V125();
            case 777181:
                return new V777181();
            default:
                return null;
        }
    }

}
abstract class Version
{
    protected String name;
    protected String[] libs;
    protected String[] natives;
    protected String jar;
    protected String assetIndex;
    protected String assets;
    protected String lib;
}
class V152 extends Version
{
    public V152()
    {
        super();
        this.name = "1.5.2";
        this.jar = "https://launcher.mojang.com/v1/objects/465378c9dc2f779ae1d6e8046ebc46fb53a57968/client.jar";
        this.assetIndex = "pre-1.6";
        this.assets = "https://codeberg.org/glowiak/glm-assets/raw/branch/master/1.5.2/assets.zip";
        this.lib = "https://codeberg.org/glowiak/glm-assets/raw/branch/master/1.5.2/lib.zip";
    
        String[] libs1 = {
        "https://libraries.minecraft.net/net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar",
        "https://libraries.minecraft.net/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar",
        "https://libraries.minecraft.net/org/ow2/asm/asm-all/4.1/asm-all-4.1.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar",
        "https://libraries.minecraft.net/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl/2.9.0/lwjgl-2.9.0.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl_util/2.9.0/lwjgl_util-2.9.0.jar"
    }; this.libs = libs1;
        String[] natives1 = {
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-linux.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-osx.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-windows.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-linux.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-osx.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-windows.jar"
    }; this.natives = natives1;
    }
}
class V125 extends Version
{
    public V125()
    {
        super();
        this.name = "1.2.5";
        this.jar = "https://launcher.mojang.com/v1/objects/4a2fac7504182a97dcbcd7560c6392d7c8139928/client.jar";
        this.assetIndex = "pre-1.6";
        this.assets = "https://codeberg.org/glowiak/glm-assets/raw/branch/master/1.5.2/assets.zip";
        this.lib = null;
        
        String[] libs1 = {
        "https://libraries.minecraft.net/net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar",
        "https://libraries.minecraft.net/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar",
        "https://libraries.minecraft.net/org/ow2/asm/asm-all/4.1/asm-all-4.1.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar",
        "https://libraries.minecraft.net/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl/2.9.0/lwjgl-2.9.0.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl_util/2.9.0/lwjgl_util-2.9.0.jar"
    }; this.libs = libs1;
        String[] natives1 = {
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-linux.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-osx.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-windows.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-linux.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-osx.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-windows.jar"
    }; this.natives = natives1;
    }
}
class V777181 extends Version
{
    public V777181()
    {
        this.name = "b1.8.1";
        this.jar = "https://launcher.mojang.com/v1/objects/6b562463ccc2c7ff12ff350a2b04a67b3adcd37b/client.jar";
        this.assetIndex = "pre-1.6";
        this.assets = "https://codeberg.org/glowiak/glm-assets/raw/branch/master/1.5.2/assets.zip";
        this.lib = null;
        
        String[] libs1 = {
        "https://libraries.minecraft.net/net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar",
        "https://libraries.minecraft.net/net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar",
        "https://libraries.minecraft.net/org/ow2/asm/asm-all/4.1/asm-all-4.1.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput/2.0.5/jinput-2.0.5.jar",
        "https://libraries.minecraft.net/net/java/jutils/jutils/1.0.0/jutils-1.0.0.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl/2.9.0/lwjgl-2.9.0.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl_util/2.9.0/lwjgl_util-2.9.0.jar"
    }; this.libs = libs1;
        String[] natives1 = {
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-linux.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-osx.jar",
        "https://libraries.minecraft.net/org/lwjgl/lwjgl/lwjgl-platform/2.9.0/lwjgl-platform-2.9.0-natives-windows.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-linux.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-osx.jar",
        "https://libraries.minecraft.net/net/java/jinput/jinput-platform/2.0.5/jinput-platform-2.0.5-natives-windows.jar"
    }; this.natives = natives1;
    }
}
